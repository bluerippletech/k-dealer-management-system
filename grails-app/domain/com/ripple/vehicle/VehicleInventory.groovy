package com.ripple.vehicle

import org.joda.time.LocalDate

class VehicleInventory {
    VehicleModel    model
    Integer         yearModel
    String          csNumber
    BigDecimal      amount
    BigDecimal      amountPaid

    LocalDate       dateGranted
    LocalDate       dateDelivered

    String          color
    String          notes



    static constraints = {
        model(blank:false,nullable: false)
        yearModel(min: 1940,max: 2100,blank:false,nullable:false)
        csNumber(blank: false,nullable:false)
        amount(min: 0.00, blank:false,nullable:false)
        amountPaid(min:0.00,blank:false,nullable:false)
        dateGranted(blank:false,nullable: false)
        dateDelivered(blank:false,nullable: false)
        color(blank: false,nullable:false)
        notes(blank: true,nullable: true,maxSize: 1000)

    }

    static transients = ['age','remainingBalance']


    def getAge(){
        (new LocalDate()).minus(dateGranted)
    }

    def getRemainingBalance(){
        amount - amountPaid
    }

    static namedQueries = {

        lessThan30Days {
            def now = new LocalDate()
            gt 'dateGranted', now.minusDays(30)
        }

        thirtyToSixtyDays {
            def now = new LocalDate()
            between 'dateGranted', now.minusDays(60),now.minusDays(31)

        }

        sixtyToSeventyFiveDays {
            def now = new LocalDate()
            between 'dateGranted', now.minusDays(75),now.minusDays(61)
        }

        seventysixToNinetyDays {
            def now = new LocalDate()
            between 'dateGranted', now.minusDays(90),now.minusDays(76)

        }

        above91Days{
            def now = new LocalDate()
            lte 'dateGranted',now.minusDays(91)
        }

    }


}
