package com.ripple.vehicle

class VehicleModel {

    String modelCode
    String description


    static constraints = {
        modelCode(blank: false,nullable: false)
        description(blank:false,nullable: false)

    }

    String toString(){
        return "${modelCode} - ${description}";
    }
}
