<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <title>Admin Simplenso - Dashboard</title>
    <r:require modules="application,bootstrapjs,jquery,all"/>
    <!-- Bootstrap -->
    %{--<link href="bootstrap/css/bootstrap.css" rel="stylesheet" id="main-theme-script">--}%
    %{--<link href="css/themes/default.css" rel="stylesheet" id="theme-specific-script">--}%
    %{--<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">--}%

</head>

<body id="dashboard">

<!-- Geographic Page Visit Map -->
<div class="row-fluid">
    <div><div id="dashboard-visit-map"></div></div>
</div>

<div class="row-fluid">
    <!-- Portlet Set 1 -->
    <div class="span4 column" id="col1">
        <!-- Portlet: Browser Usage Graph -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top">Browser Usage Graph
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <div id="dashboard-browser-chart"></div>
                </div>
            </div>
        </div><!--/span-->
    </div>

    <!-- Portlet Set 2 -->
    <div class="span4 column" id="col2">
        <!-- Portlet: Page Visit Graph -->
        <div class="box" id="box-1">
            <h4 class="box-header round-top">Page Visit Graph
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <div id="dashboard-visit-chart"></div>
                </div>
            </div>
        </div><!--/span-->
    </div>

    <!-- Portlet Set 3 -->
    <div class="span4 column" id="col3">
        <!-- Portlet: Site Activity Gauges -->
        <div class="box" id="box-2">
            <h4 class="box-header round-top">Site Activity Gauges
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <div id="dashboard-new-registrations-gauge-chart"></div>
                </div>
            </div>
        </div><!--/span-->
    </div>
</div>


<div id="box-config-modal" class="modal hide fade in" style="display: none;">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>

        <h3>Adjust widget</h3>
    </div>

    <div class="modal-body">
        <p>This part can be customized to set box content specifix settings!</p>
    </div>

    <div class="modal-footer">
        <a href="#" class="btn btn-primary" data-dismiss="modal">Save Changes</a>
        <a href="#" class="btn" data-dismiss="modal">Cancel</a>
    </div>
</div>
<!-- javascript Templates
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<!-- Le javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Google API -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<!-- MultiFile Upload -->
<!-- Error messages for the upload/download templates -->
<script>
    var fileUploadErrors = {
        maxFileSize:'File is too big',
        minFileSize:'File is too small',
        acceptFileTypes:'Filetype not allowed',
        maxNumberOfFiles:'Max number of files exceeded',
        uploadedBytes:'Uploaded bytes exceed file size',
        emptyResult:'Empty file upload result'
    };
</script>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/html">
    {% for (var i=0, files=o.files, l=files.length, file=files[0]; i<l; file=files[++i]) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name">{%=file.name%}</td>
        <td class="size">{%=o.formatFileSize(file.size)%}</td>
        {% if (file.error) { %}
        <td class="error" colspan="2"><span
                class="label label-important">Error</span> {%=fileUploadErrors[file.error] || file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
        <td>
            <div class="progress progress-success progress-striped active"><div class="bar" style="width:0%;"></div>
            </div>
        </td>
        <td class="start">{% if (!o.options.autoUpload) { %}
            <button class="btn btn-primary">
                <i class="icon-upload icon-white"></i> Start
            </button>
            {% } %}</td>
        {% } else { %}
        <td colspan="2"></td>
        {% } %}
        <td class="cancel">{% if (!i) { %}
            <button class="btn btn-warning">
                <i class="icon-ban-circle icon-white"></i> Cancel
            </button>
            {% } %}</td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/html">
    {% for (var i=0, files=o.files, l=files.length, file=files[0]; i<l; file=files[++i]) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
        <td></td>
        <td class="name">{%=file.name%}</td>
        <td class="size">{%=o.formatFileSize(file.size)%}</td>
        <td class="error" colspan="2"><span
                class="label label-important">Error</span> {%=fileUploadErrors[file.error] || file.error%}</td>
        {% } else { %}
        <td class="preview">{% if (file.thumbnail_url) { %}
            <a href="{%=file.url%}" title="{%=file.name%}" rel="gallery"><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
        <td class="name">
            <a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_url&&'gallery'%}">{%=file.name%}</a>
        </td>
        <td class="size">{%=o.formatFileSize(file.size)%}</td>
        <td colspan="2"></td>
        {% } %}
        <td class="delete">
            <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}">
                <i class="icon-trash icon-white"></i> Delete
            </button>
            <input type="checkbox" name="delete" value="1">
        </td>
    </tr>
    {% } %}
</script>

</body>
</html>
