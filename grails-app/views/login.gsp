<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admin Simplenso - Dashboard</title>
    <r:require modules="application,bootstrapjs,jquery,all,highcharts"/>
    <r:layoutResources/>
</head>

<body id="dashboard">
<!-- Main Content Area | Side Nav | Content -->
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span4">&nbsp;</div>
        <div class="span4">
            <div class="page-header">
                <h1>Kia DMS Login</h1>
            </div>
            <div class="box" style="margin-bottom:500px;">
                <h4 class="box-header round-top">Kia DMS Login</h4>
                <div class="box-container-toggle">
                    <div class="box-content">
                        <form class="well form-search" action="${createLink(uri:"/")}">
                            <input type="text" class="input-small" placeholder="Email">
                            <input type="password" class="input-small" placeholder="Password">
                            <button type="submit" class="btn">Go</button>
                            <label class="inline uniform" for="remember" style="vertical-align: bottom;">
                                <input class="uniform_on" type="checkbox" id="remember" value="option1">
                                Remember me? </label>
                        </form>
                    </div>
                </div>
            </div>
            <!--/span-->
            <div class="span4">&nbsp;</div>
        </div>
        <!--/span-->
    </div>
    <!--/row-->

    <footer>
        <p class="pull-right">&copy; Simplenso 2012</p>
    </footer>
</div>

<r:layoutResources/>
</body>
</html>
