<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <title>Admin Simplenso - Dashboard</title>
    <r:require modules="all"/>

</head>

<body id="dashboard">
<g:render template="/layouts/navIcons" />

<div class="row-fluid">
    <!-- Portlet -->

    <div class="span12 column" id="col0">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top">Form control states
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <form class="form-horizontal">
                        <fieldset>
                            <legend>Form control states</legend>

                            <div class="control-group">
                                <label class="control-label" for="focusedInput">Focused input</label>

                                <div class="controls">
                                    <input class="input-xlarge focused" id="focusedInput" type="text"
                                           value="This is focused…">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Uneditable input</label>

                                <div class="controls">
                                    <span class="input-xlarge uneditable-input">Some value here</span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="disabledInput">Disabled input</label>

                                <div class="controls">
                                    <input class="input-xlarge disabled" id="disabledInput" type="text"
                                           placeholder="Disabled input here…" disabled="">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="optionsCheckbox2">Disabled checkbox</label>

                                <div class="controls">
                                    <label>
                                        <input type="checkbox" id="optionsCheckbox2" value="option1" disabled="">
                                        This is a disabled checkbox
                                    </label>
                                </div>
                            </div>

                            <div class="control-group warning">
                                <label class="control-label" for="inputError">Input with warning</label>

                                <div class="controls">
                                    <input type="text" id="inputError">
                                    <span class="help-inline">Something may have gone wrong</span>
                                </div>
                            </div>

                            <div class="control-group error">
                                <label class="control-label" for="inputError">Input with error</label>

                                <div class="controls">
                                    <input type="text" id="inputError">
                                    <span class="help-inline">Please correct the error</span>
                                </div>
                            </div>

                            <div class="control-group success">
                                <label class="control-label" for="inputError">Input with success</label>

                                <div class="controls">
                                    <input type="text" id="inputError">
                                    <span class="help-inline">Woohoo!</span>
                                </div>
                            </div>

                            <div class="control-group success">
                                <label class="control-label" for="selectError">Select with success</label>

                                <div class="controls">
                                    <select id="selectError">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                    <span class="help-inline">Woohoo!</span>
                                </div>
                            </div>

                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save changes</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div><!--/span-->
    </div>
</div>

</body>
</html>
