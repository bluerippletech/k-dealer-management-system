<%@ page import="com.ripple.vehicle.VehicleModel" %>



<div class="fieldcontain ${hasErrors(bean: vehicleModelInstance, field: 'modelCode', 'error')} required">
	<label for="modelCode">
		<g:message code="vehicleModel.modelCode.label" default="Model Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="modelCode" required="" value="${vehicleModelInstance?.modelCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: vehicleModelInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="vehicleModel.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${vehicleModelInstance?.description}"/>
</div>

