
<%@ page import="com.ripple.vehicle.VehicleModel" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'vehicleModel.label', default: 'VehicleModel')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-vehicleModel" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                            
                            <g:if test="${vehicleModelInstance?.modelCode}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="modelCode-label" class="control-label"><g:message
                                            code="vehicleModel.modelCode.label"
                                            default="Model Code"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="modelCode-label"><g:fieldValue
                                            bean="${vehicleModelInstance}" field="modelCode"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${vehicleModelInstance?.description}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="description-label" class="control-label"><g:message
                                            code="vehicleModel.description.label"
                                            default="Description"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="description-label"><g:fieldValue
                                            bean="${vehicleModelInstance}" field="description"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${vehicleModelInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${vehicleModelInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
