<!-- Side Navigation -->
<div class="span2">
    <div class="member-box round-all">
        <a><r:img uri="/images/butch.png" class="member-box-avatar" /></a>
        <span>
            <strong>Administrator</strong><br/>
            <a>Butch Camacho</a><br/>
            <span class="member-box-links"><a>Settings</a> | <a>Logout</a></span>
        </span>
    </div>
    <div class="sidebar-nav">
        <div class="well" style="padding: 8px 0;">
            <ul class="nav nav-list">
                <li class="nav-header">Vehicle Management</li>
                <li class="${params.controller?.equals("vehicleModel")?"active":""}"><a href="${createLink(controller:'vehicleModel',action:"list")}"><i class="icon-road"></i>Vehicle Models</a></li>
                <li  class="${params.controller?.equals("vehicleInventory")?"active":""}"><a href="${createLink(controller:'vehicleInventory',action:"list")}"><i class="icon-list"></i>Vehicle Inventory</a></li>
                %{--<li><a href="members.html"><i class="icon-user"></i> Members</a></li>--}%
                %{--<li><a href="comments.html"><i class="icon-comment"></i> Comments</a></li>--}%
                %{--<li><a href="gallery.html"><i class="icon-picture"></i> Gallery</a></li>--}%
                %{--<li><a href="calendar.html"><i class="icon-calendar"></i> Calendar</a></li>--}%
                <li class="nav-header">Management</li>
                <li><a href="${createLink(uri:'/')}"><i class="icon-home"></i>Dashboard</a></li>
                %{--<li><a href="grid.html"><i class="icon-th-large"></i> Grid</a></li>--}%
                %{--<li><a href="portlets.html"><i class="icon-th"></i> Portlets</a></li>--}%
                %{--<li><a href="forms.html"><i class="icon-th"></i> Forms</a></li>--}%
                %{--<li><a href="tables.html"><i class="icon-align-justify"></i> Tables</a></li>--}%
                %{--<li><a href="other.html"><i class="icon-gift"></i> Other</a></li>--}%
                <li class="nav-header">Settings</li>
                <li><a class="cookie-delete" href="#"><i class="icon-wrench"></i> Delete Cookies</a></li>
                <li><a class="sidenav-style-1" href="#"><i class="icon-align-left"></i> Side Menu Style 1</a></li>
                <li><a class="sidenav-style-2" href="#"><i class="icon-align-right"></i> Side Menu Style 2</a></li>
                <li><a href="login.html"><i class="icon-off"></i> Logout</a></li>
            </ul>
        </div>
    </div><!--/.well -->
</div><!--/span-->
