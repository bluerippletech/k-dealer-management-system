<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="#">
            <div><i class="icon-pencil"></i></div>
            <div><strong>Add</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="#">
            <div><i class="icon-edit"></i></div>
            <div><strong>Edit</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="#">
            <div><i class="icon-trash"></i></div>
            <div><strong>Delete</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="#">
            <div><i class="icon-align-justify"></i></div>
            <div><strong>List</strong></div>
        </a>
    </div>
</div>


%{--<a href="#create-${domainClass.propertyName}" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--<div class="nav" role="navigation">--}%
    %{--<ul>--}%
        %{--<li><a class="home" href="\${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
        %{--<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
    %{--</ul>--}%
%{--</div>--}%