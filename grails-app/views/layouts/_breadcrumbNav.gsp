<!-- Bread Crumb Navigation -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="${createLink(uri:'/')}">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Library</a> <span class="divider">/</span>
        </li>
        <li class="active">Data</li>
    </ul>
</div>
