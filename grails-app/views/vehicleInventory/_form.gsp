<%@ page import="com.ripple.vehicle.VehicleInventory" %>



<div class="fieldcontain ${hasErrors(bean: vehicleInventoryInstance, field: 'model', 'error')} required">
	<label for="model">
		<g:message code="vehicleInventory.model.label" default="Model" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="model" name="model.id" from="${com.ripple.vehicle.VehicleModel.list()}" optionKey="id" required="" value="${vehicleInventoryInstance?.model?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: vehicleInventoryInstance, field: 'yearModel', 'error')} required">
	<label for="yearModel">
		<g:message code="vehicleInventory.yearModel.label" default="Year Model" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="yearModel" min="1940" max="2100" required="" value="${vehicleInventoryInstance.yearModel}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: vehicleInventoryInstance, field: 'csNumber', 'error')} required">
	<label for="csNumber">
		<g:message code="vehicleInventory.csNumber.label" default="Cs Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="csNumber" required="" value="${vehicleInventoryInstance?.csNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: vehicleInventoryInstance, field: 'amount', 'error')} required">
	<label for="amount">
		<g:message code="vehicleInventory.amount.label" default="Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="amount" step="any" required="" value="${vehicleInventoryInstance.amount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: vehicleInventoryInstance, field: 'dateGranted', 'error')} required">
	<label for="dateGranted">
		<g:message code="vehicleInventory.dateGranted.label" default="Date Granted" />
		<span class="required-indicator">*</span>
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: vehicleInventoryInstance, field: 'dateDelivered', 'error')} required">
	<label for="dateDelivered">
		<g:message code="vehicleInventory.dateDelivered.label" default="Date Delivered" />
		<span class="required-indicator">*</span>
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: vehicleInventoryInstance, field: 'color', 'error')} required">
	<label for="color">
		<g:message code="vehicleInventory.color.label" default="Color" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="color" required="" value="${vehicleInventoryInstance?.color}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: vehicleInventoryInstance, field: 'notes', 'error')} ">
	<label for="notes">
		<g:message code="vehicleInventory.notes.label" default="Notes" />
		
	</label>
	<g:textField name="notes" value="${vehicleInventoryInstance?.notes}"/>
</div>

