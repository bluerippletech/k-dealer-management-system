
<%@ page import="com.ripple.vehicle.VehicleInventory" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'vehicleInventory.label', default: 'VehicleInventory')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-vehicleInventory" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                            
                            <g:if test="${vehicleInventoryInstance?.model}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="model-label" class="control-label"><g:message
                                            code="vehicleInventory.model.label"
                                            default="Model"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="model-label"><g:link
                                            controller="vehicleModel" action="show"
                                            id="${vehicleInventoryInstance?.model?.id}">${vehicleInventoryInstance?.model?.encodeAsHTML()}</g:link></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${vehicleInventoryInstance?.yearModel}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="yearModel-label" class="control-label"><g:message
                                            code="vehicleInventory.yearModel.label"
                                            default="Year Model"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="yearModel-label"><g:fieldValue
                                            bean="${vehicleInventoryInstance}" field="yearModel"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${vehicleInventoryInstance?.csNumber}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="csNumber-label" class="control-label"><g:message
                                            code="vehicleInventory.csNumber.label"
                                            default="Cs Number"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="csNumber-label"><g:fieldValue
                                            bean="${vehicleInventoryInstance}" field="csNumber"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${vehicleInventoryInstance?.amount}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="amount-label" class="control-label"><g:message
                                            code="vehicleInventory.amount.label"
                                            default="Amount"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="amount-label"><g:fieldValue
                                            bean="${vehicleInventoryInstance}" field="amount"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${vehicleInventoryInstance?.dateGranted}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="dateGranted-label" class="control-label"><g:message
                                            code="vehicleInventory.dateGranted.label"
                                            default="Date Granted"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="dateGranted-label"><g:fieldValue
                                            bean="${vehicleInventoryInstance}" field="dateGranted"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${vehicleInventoryInstance?.dateDelivered}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="dateDelivered-label" class="control-label"><g:message
                                            code="vehicleInventory.dateDelivered.label"
                                            default="Date Delivered"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="dateDelivered-label"><g:fieldValue
                                            bean="${vehicleInventoryInstance}" field="dateDelivered"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${vehicleInventoryInstance?.color}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="color-label" class="control-label"><g:message
                                            code="vehicleInventory.color.label"
                                            default="Color"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="color-label"><g:fieldValue
                                            bean="${vehicleInventoryInstance}" field="color"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${vehicleInventoryInstance?.notes}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="notes-label" class="control-label"><g:message
                                            code="vehicleInventory.notes.label"
                                            default="Notes"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="notes-label"><g:fieldValue
                                            bean="${vehicleInventoryInstance}" field="notes"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${vehicleInventoryInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${vehicleInventoryInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
