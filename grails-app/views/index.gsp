<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <title>Admin Simplenso - Dashboard</title>
    <r:require modules="application,bootstrapjs,jquery,all,highcharts"/>
    <r:script>
           var chart;
           var chart1;
           var chart2;
           var options;
           var options1;
           var options2;
        function requestInventoryData() {
            $.ajax({
                url: '${createLink(controller:'chart',action:'getInventoryAgingUnits')}',
                success: function(point) {
                   options.series[0].data=point;
                   chart = new Highcharts.Chart(options);

                    // call it again after one second
//                    setTimeout(requestData, 1000);
                },
                cache: false
            });
        }
        function requestInventoryValue() {
            $.ajax({
                url: '${createLink(controller:'chart',action:'getInventoryAgingValues')}',
                success: function(point) {
                   options1.series[0].data=point;
                   chart1 = new Highcharts.Chart(options1);

                    // call it again after one second
//                    setTimeout(requestData, 1000);
                },
                cache: false
            });
        }
              function requestInventoryTrust() {
            $.ajax({
                url: '${createLink(controller:'chart',action:'getInventoryTrustReceipts')}',
                success: function(point) {
                   options2.series[0].data=point;
                   chart2 = new Highcharts.Chart(options2);

                    // call it again after one second
//                    setTimeout(requestData, 1000);
                },
                cache: false
            });
        }


        $(function () {

            $(document).ready(function() {
                         requestInventoryData();
                         requestInventoryValue();
                         requestInventoryTrust();


               options={
                    chart: {
                        renderTo: 'container',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        events:{
//                            load:requestInventoryData
                        }
                    },
                    title: {
                        text: 'Inventory Aging (in Units) Units'
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                                }
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        data: []
                    }]
                }
                options1={
                    chart: {
                        renderTo: 'container1',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        events:{
//                            load:requestInventoryData
                        }
                    },
                    title: {
                        text: 'Inventory Aging (in Value) Units'
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>'+ this.point.y +'k</b>';
                        }
                    },
                    xAxis: {
                categories: ['0-30 Days', '31-60 Days', '61-75 Days', '76-90 Days', '91+ Days'],
                title: {
                    text: null
                }
                     },
                    yAxis: {
                       min: 0,
                      title: {
                         text: 'Value (thousands)',
                         align: 'high'
                          }
                      },
                    plotOptions: {
                        bar: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>'+ this.point.y +'k</b>';
                                }
                            }
                        }
                    },
                    series: [{
                        type: 'bar',
                        name: 'Days',
                        data: []
                    }]
                }
                options2={
                    chart: {
                        renderTo: 'container2',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        events:{
//                            load:requestInventoryData
                        }
                    },
                    title: {
                        text: 'Aging of Trust Receipts (in Value) Units'
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>'+ this.point.y +'k</b>';
                        }
                    },
                    xAxis: {
                categories: ['0-30 Days', '31-60 Days', '61-75 Days', '76-90 Days', '91+ Days'],
                title: {
                    text: null
                }
                     },
                    yAxis: {
                       min: 0,
                      title: {
                         text: 'Value (thousands)',
                         align: 'high'
                          }
                      },

                    plotOptions: {
                        column: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>'+ this.point.y +'k</b>';
                                }
                            }
                        }
                    },
                    series: [{
                        type: 'column',
                        name: 'Days',
                        data: []
                    }]
                }
            });

        });

    </r:script>
</head>

<body id="dashboard">

<!-- Geographic Page Visit Map -->
<div class="row-fluid">
    <div><div id="dashboard-visit-map"></div></div>
</div>

<div class="row-fluid">
    <!-- Portlet Set 1 -->
    <div class="span12 column" id="col1">
        <!-- Portlet: Browser Usage Graph -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top">Inventory Aging (in Units)
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div><!--/span-->
    </div>
</div>
<div class="row-fluid">
    <!-- Portlet Set 2 -->
    <div class="span6 column" id="col2">
        <!-- Portlet: Page Visit Graph -->
        <div class="box" id="box-1">
            <h4 class="box-header round-top">Inventory Aging (in Value)
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <div id="container1" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div><!--/span-->
    </div>

    <!-- Portlet Set 3 -->
    <div class="span6 column" id="col3">
        <!-- Portlet: Site Activity Gauges -->
        <div class="box" id="box-2">
            <h4 class="box-header round-top">Aging of Trust Receipts (in Value)
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <div id="container2" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div><!--/span-->
    </div>
</div>


</body>
</html>
