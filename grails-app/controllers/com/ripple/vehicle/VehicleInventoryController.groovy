package com.ripple.vehicle



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class VehicleInventoryController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [vehicleInventoryInstanceList: VehicleInventory.list(params), vehicleInventoryInstanceTotal: VehicleInventory.count()]
    }

    def create() {
        [vehicleInventoryInstance: new VehicleInventory(params)]
    }

    def save() {
        def vehicleInventoryInstance = new VehicleInventory(params)
        if (!vehicleInventoryInstance.save(flush: true)) {
            render(view: "create", model: [vehicleInventoryInstance: vehicleInventoryInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'vehicleInventory.label', default: 'VehicleInventory'), vehicleInventoryInstance.id])
        redirect(action: "show", id: vehicleInventoryInstance.id)
    }

    def show() {
        def vehicleInventoryInstance = VehicleInventory.get(params.id)
        if (!vehicleInventoryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleInventory.label', default: 'VehicleInventory'), params.id])
            redirect(action: "list")
            return
        }

        [vehicleInventoryInstance: vehicleInventoryInstance]
    }

    def edit() {
        def vehicleInventoryInstance = VehicleInventory.get(params.id)
        if (!vehicleInventoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleInventory.label', default: 'VehicleInventory'), params.id])
            redirect(action: "list")
            return
        }

        [vehicleInventoryInstance: vehicleInventoryInstance]
    }

    def update() {
        def vehicleInventoryInstance = VehicleInventory.get(params.id)
        if (!vehicleInventoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleInventory.label', default: 'VehicleInventory'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (vehicleInventoryInstance.version > version) {
                vehicleInventoryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'vehicleInventory.label', default: 'VehicleInventory')] as Object[],
                          "Another user has updated this VehicleInventory while you were editing")
                render(view: "edit", model: [vehicleInventoryInstance: vehicleInventoryInstance])
                return
            }
        }

        vehicleInventoryInstance.properties = params

        if (!vehicleInventoryInstance.save(flush: true)) {
            render(view: "edit", model: [vehicleInventoryInstance: vehicleInventoryInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'vehicleInventory.label', default: 'VehicleInventory'), vehicleInventoryInstance.id])
        redirect(action: "show", id: vehicleInventoryInstance.id)
    }

    def delete() {
        def vehicleInventoryInstance = VehicleInventory.get(params.id)
        if (!vehicleInventoryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleInventory.label', default: 'VehicleInventory'), params.id])
            redirect(action: "list")
            return
        }

        try {
            vehicleInventoryInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'vehicleInventory.label', default: 'VehicleInventory'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'vehicleInventory.label', default: 'VehicleInventory'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON(){
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho)+1
        def sSearch = params.sSearch

        def listing = VehicleInventory.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
            order(sortColumn,sortOrder)
            if (sSearch){
                or{
                    
                            ilike('model',"%${sSearch}%")
                            
                            ilike('yearModel',"%${sSearch}%")
                            
                            ilike('csNumber',"%${sSearch}%")
                            
                            ilike('amount',"%${sSearch}%")
                            
                            ilike('dateGranted',"%${sSearch}%")
                            
                            ilike('dateDelivered',"%${sSearch}%")
                            
                }
            }
        }
        def totalRecords = VehicleInventory.count();
        def totalDisplayRecords = VehicleInventory.createCriteria().count{
            if (sSearch){
                or{
                    
                        ilike('model',"%${sSearch}%")
                        
                        ilike('yearModel',"%${sSearch}%")
                        
                        ilike('csNumber',"%${sSearch}%")
                        
                        ilike('amount',"%${sSearch}%")
                        
                        ilike('dateGranted',"%${sSearch}%")
                        
                        ilike('dateDelivered',"%${sSearch}%")
                        
                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [
                
                    0:it.model.toString(),
                    
                    1:it.yearModel,
                    
                    2:it.csNumber,
                    
                    3:it.amount,
                    
                    4:it.dateGranted,
                    
                    5:it.dateDelivered,
                    
                    6:'',
                    "DT_RowId": it.id
                    
            ]
        }
        def data = [
                sEcho:sEcho,
                iTotalRecords:totalRecords,
                iTotalDisplayRecords:totalDisplayRecords,
                aaData:jsonList];
        render data as JSON;
    }
}
