package com.ripple.vehicle

import grails.converters.JSON

class ChartController {

    def index() { }


    def getInventoryAgingUnits(){

        def data =[['0-30 Days', VehicleInventory.lessThan30Days.count()],
                    ['31-60 Days', VehicleInventory.thirtyToSixtyDays.count()],
                    ['61-75 Days', VehicleInventory.sixtyToSeventyFiveDays.count()],
                    ['76-90 Days', VehicleInventory.seventysixToNinetyDays.count()],
                    ['91+ Days', VehicleInventory.above91Days.count()]]


        render (data as JSON)
    }

    def getInventoryAgingValues(){

        def data =[['0-30 Days', (VehicleInventory.lessThan30Days.list().sum{it.amount}?:0)/1000],
                ['31-60 Days', (VehicleInventory.thirtyToSixtyDays.list().sum{it.amount}?:0)/1000],
                ['61-75 Days', (VehicleInventory.sixtyToSeventyFiveDays.list().sum{it.amount}?:0)/1000],
                ['76-90 Days', (VehicleInventory.seventysixToNinetyDays.list().sum{it.amount}?:0)/1000],
                ['91+ Days', (VehicleInventory.above91Days.list().sum{it.amount}?:0)/1000]]


        render (data as JSON)
    }

    def getInventoryTrustReceipts(){

        def data =[['0-30 Days', (VehicleInventory.lessThan30Days.list().findAll{it.amountPaid > 0}.sum{it.amountPaid>0?it.amount-it.amountPaid:it.amount}?:0)/1000],
                ['31-60 Days', (VehicleInventory.thirtyToSixtyDays.list().findAll{it.amountPaid > 0}.sum{it.amountPaid>0?it.amount-it.amountPaid:it.amount}?:0)/1000],
                ['61-75 Days', (VehicleInventory.sixtyToSeventyFiveDays.list().findAll{it.amountPaid > 0}.sum{it.amountPaid>0?it.amount-it.amountPaid:it.amount}?:0)/1000],
                ['76-90 Days', (VehicleInventory.seventysixToNinetyDays.list().findAll{it.amountPaid > 0}.sum{it.amountPaid>0?it.amount-it.amountPaid:it.amount}?:0)/1000],
                ['91+ Days', (VehicleInventory.above91Days.list().findAll{it.amountPaid > 0}.sum{it.amountPaid>0?it.amount-it.amountPaid:it.amount}?:0)/1000]]


        render (data as JSON)
    }
}
