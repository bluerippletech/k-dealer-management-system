package com.ripple.vehicle

import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class VehicleModelController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [vehicleModelInstanceList: VehicleModel.list(params), vehicleModelInstanceTotal: VehicleModel.count()]
    }

    def create() {
        [vehicleModelInstance: new VehicleModel(params)]
    }

    def save() {
        def vehicleModelInstance = new VehicleModel(params)
        if (!vehicleModelInstance.save(flush: true)) {
            render(view: "create", model: [vehicleModelInstance: vehicleModelInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'vehicleModel.label', default: 'VehicleModel'), vehicleModelInstance.id])
        redirect(action: "show", id: vehicleModelInstance.id)
    }

    def show() {
        def vehicleModelInstance = VehicleModel.get(params.id)
        if (!vehicleModelInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleModel.label', default: 'VehicleModel'), params.id])
            redirect(action: "list")
            return
        }

        [vehicleModelInstance: vehicleModelInstance]
    }

    def edit() {
        def vehicleModelInstance = VehicleModel.get(params.id)
        if (!vehicleModelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleModel.label', default: 'VehicleModel'), params.id])
            redirect(action: "list")
            return
        }

        [vehicleModelInstance: vehicleModelInstance]
    }

    def update() {
        def vehicleModelInstance = VehicleModel.get(params.id)
        if (!vehicleModelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleModel.label', default: 'VehicleModel'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (vehicleModelInstance.version > version) {
                vehicleModelInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'vehicleModel.label', default: 'VehicleModel')] as Object[],
                          "Another user has updated this VehicleModel while you were editing")
                render(view: "edit", model: [vehicleModelInstance: vehicleModelInstance])
                return
            }
        }

        vehicleModelInstance.properties = params

        if (!vehicleModelInstance.save(flush: true)) {
            render(view: "edit", model: [vehicleModelInstance: vehicleModelInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'vehicleModel.label', default: 'VehicleModel'), vehicleModelInstance.id])
        redirect(action: "show", id: vehicleModelInstance.id)
    }

    def delete() {
        def vehicleModelInstance = VehicleModel.get(params.id)
        if (!vehicleModelInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleModel.label', default: 'VehicleModel'), params.id])
            redirect(action: "list")
            return
        }

        try {
            vehicleModelInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'vehicleModel.label', default: 'VehicleModel'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'vehicleModel.label', default: 'VehicleModel'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON(){
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho)+1
        def sSearch = params.sSearch

        def listing = VehicleModel.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
            order(sortColumn,sortOrder)
            if (sSearch){
                or{
                    ilike('description',"%${sSearch}%")
                    ilike('modelCode',"%${sSearch}%")
                }
            }

        }
        def totalRecords = VehicleModel.count();
        def totalDisplayRecords = VehicleModel.createCriteria().count{
            if (sSearch){
                or{
                    ilike('description',"%${sSearch}%")
                    ilike('modelCode',"%${sSearch}%")
                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [
                    0:it.modelCode,
                    1:it.description,
                    2:'',
                    "DT_RowId": it.id
            ]
        }
        def data = [
                sEcho:sEcho,
                iTotalRecords:totalRecords,
                iTotalDisplayRecords:totalDisplayRecords,
                aaData:jsonList];
        render data as JSON;
    }
}
