class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
        "/login"(view:"/login")
        "/forms"(view:"/forms")
        "/sample"(view: "/sample")
		"/"(view:"/index")
		"500"(view:'/error')

	}
}
